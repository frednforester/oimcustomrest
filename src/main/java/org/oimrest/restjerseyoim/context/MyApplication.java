/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.context;

import org.apache.log4j.Logger;
import org.glassfish.jersey.server.ResourceConfig;
import org.oimrest.restjerseyoim.filter.AuthFilter;

/**
 *
 * @author Owner
 */
public class MyApplication extends ResourceConfig {
    
    private static final Logger logger = Logger.getLogger(MyApplication.class.getName());
    
    public MyApplication() {
        logger.debug("*********************** MyApplication ****************8");
        //register(RolesAllowedDynamicFeature.class);
        register(AuthFilter.class);
        register(new MyApplicationBinder()); 
    }
    
}
