/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.context;

import org.apache.log4j.Logger;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.oimrest.restjerseyoim.oim.IOIMClientFactory;
import org.oimrest.restjerseyoim.oim.IOIMClientWrapper;
import org.oimrest.restjerseyoim.oim.OIMClientFactory;
import org.oimrest.restjerseyoim.oim.OIMClientWrapper;
import org.oimrest.restjerseyoim.oimservices.IMembershipOperations;
import org.oimrest.restjerseyoim.oimservices.IPasswordOperations;
import org.oimrest.restjerseyoim.oimservices.IRoleOperations;
import org.oimrest.restjerseyoim.oimservices.ITesterOperations;
import org.oimrest.restjerseyoim.oimservices.IUserOperations;
import org.oimrest.restjerseyoim.oimservices.MembershipOperationsImpl;
import org.oimrest.restjerseyoim.oimservices.PasswordOperationsImpl;
import org.oimrest.restjerseyoim.oimservices.RoleOperationsImpl;
import org.oimrest.restjerseyoim.oimservices.TesterOperationsImpl;
import org.oimrest.restjerseyoim.oimservices.UserOperationsImpl;

/**
 *
 * @author Owner
 */
public class MyApplicationBinder extends AbstractBinder {
    
    private static final Logger logger = Logger.getLogger(MyApplicationBinder.class.getName());
    
    @Override
    protected void configure() {
        logger.debug("*********************** MyApplicationBinder ****************8");
        bind(OIMClientFactory.class).to(IOIMClientFactory.class);
        bind(OIMClientWrapper.class).to(IOIMClientWrapper.class);
        bind(UserOperationsImpl.class).to(IUserOperations.class);
        bind(RoleOperationsImpl.class).to(IRoleOperations.class);
        bind(MembershipOperationsImpl.class).to(IMembershipOperations.class);
        bind(TesterOperationsImpl.class).to(ITesterOperations.class);
        bind(PasswordOperationsImpl.class).to(IPasswordOperations.class);
    }
    
}
