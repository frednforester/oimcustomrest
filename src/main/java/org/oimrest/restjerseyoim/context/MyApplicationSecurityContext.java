/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.context;

import java.security.Principal;
import javax.ws.rs.core.SecurityContext;
import oracle.iam.platform.OIMClient;
import org.oimrest.restjerseyoim.model.UserPrincipal;

/**
 *
 * @author Owner
 */
public class MyApplicationSecurityContext implements SecurityContext {
    
    
    private UserPrincipal user;
    private String scheme;
    private OIMClient client;
 
    public MyApplicationSecurityContext(UserPrincipal user, String scheme) {
        this.user = user;
        this.scheme = scheme;
    }

    @Override
    public Principal getUserPrincipal() {
        return this.user;
    }

    @Override
    public boolean isUserInRole(String string) {
        if (user.getRole() != null) {
            return user.getRole().contains(string);
        }
        return false;
    }

    @Override
    public boolean isSecure() {
        return "https".equals(this.scheme);
    }

    @Override
    public String getAuthenticationScheme() {
        return SecurityContext.BASIC_AUTH;
    }

    public OIMClient getClient() {
        return client;
    }

    public void setClient(OIMClient client) {
        this.client = client;
    }
    
    
    
}
