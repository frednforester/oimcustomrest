/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.context.rsql;

import cz.jirutka.rsql.parser.ast.AndNode;
import cz.jirutka.rsql.parser.ast.ComparisonNode;
import cz.jirutka.rsql.parser.ast.LogicalNode;
import cz.jirutka.rsql.parser.ast.LogicalOperator;
import cz.jirutka.rsql.parser.ast.Node;
import cz.jirutka.rsql.parser.ast.OrNode;
import java.util.ArrayList;
import java.util.List;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import org.apache.log4j.Logger;

/**
 *
 * @author Owner
 */
public class SearchCriteriaBuilder<T> {
    
    private static final Logger logger = Logger.getLogger(SearchCriteriaBuilder.class.getName());
    
    public SearchCriteria createSpecification(Node node) {
        if (node instanceof LogicalNode) {
            return createSpecification((LogicalNode) node);
        }
        if (node instanceof ComparisonNode) {
            return createSpecification((ComparisonNode) node);
        }
        return null;
    }
    
    
    public SearchCriteria createSpecification(LogicalNode logicalNode) {        
        List specs = logicalNode.getChildren();
          //.stream()
          //.map(node -> createSpecification(node))
          //.filter(Objects::nonNull)
          //.collect(Collectors.toList());
        logger.debug("LogicalNode:" + logicalNode);
        logger.debug("LogicalNode operator:" + logicalNode.getOperator());
        logger.debug("specs:" + specs);
        if (specs == null)
        {
            return null;
        }
        logger.debug("numspecs:" + specs.size());
        List<SearchCriteria> criterias = null;
        SearchCriteria first = null;
        SearchCriteria second = null;
        if (logicalNode.getOperator() == LogicalOperator.AND) {
            logger.debug("LogicalOperator And");
            criterias = this.processSpecs(specs);
            int count = 1;
            for(SearchCriteria crit : criterias)
            {
                if (count == 1)
                {
                    count++;
                    first = crit;
                    continue;
                }
                first = new SearchCriteria(first,crit,SearchCriteria.Operator.AND);
            }
        } else if (logicalNode.getOperator() == LogicalOperator.OR) {
            logger.debug("LogicalOperator Or");
            criterias = this.processSpecs(specs);
            int count = 1;
            for(SearchCriteria crit : criterias)
            {
                if (count == 1)
                {
                    count++;
                    second = crit;
                    continue;
                }
                second = new SearchCriteria(second,crit,SearchCriteria.Operator.OR);
            }
        }
        if (first != null && second != null)
        {
            SearchCriteria third = new SearchCriteria(first,second,SearchCriteria.Operator.AND);
            first = third;
        }
        else
        if (second != null)
        {
            return second;
        }
        return first;
    }
    
    public List<SearchCriteria>  processSpecs(List specs)
    {
        List<SearchCriteria>  criterias = new ArrayList();
        
        for(Object result : specs)
        {
            logger.debug("TYPE:" + result.getClass().getName());
            if (result instanceof ComparisonNode)
            {
                ComparisonNode cn = (ComparisonNode)result;
                criterias.add(this.createSpecification(cn));
            }
            if (result instanceof OrNode)
            {
                OrNode on = (OrNode)result;
                List<Node> nodes = on.getChildren();
                logger.debug("OrNode nodes:" + nodes);
                for(Node node : nodes)
                {
                    if (node instanceof ComparisonNode)
                    {
                        criterias.add(this.createSpecification(node));
                    }
                }
                int count = 1;
                SearchCriteria first = null;
                for(SearchCriteria crit : criterias)
                {
                    if (count == 1)
                    {
                        count++;
                        first = crit;
                        continue;
                    }
                    first = new SearchCriteria(first,crit,SearchCriteria.Operator.OR);
                }
                criterias.clear();
                criterias.add(first);
            }
            if (result instanceof AndNode)
            {
                AndNode an = (AndNode)result;
                List<Node> nodes = an.getChildren();
                logger.debug("AndNode nodes:" + nodes);
                for(Node node : nodes)
                {
                    if (node instanceof ComparisonNode)
                    {
                        criterias.add(this.createSpecification(node));
                    }
                }
                int count = 1;
                SearchCriteria first = null;
                for(SearchCriteria crit : criterias)
                {
                    if (count == 1)
                    {
                        count++;
                        first = crit;
                        continue;
                    }
                    first = new SearchCriteria(first,crit,SearchCriteria.Operator.AND);
                }
                criterias.clear();
                criterias.add(first);
            }
        }
        return criterias;
    }
    
    public SearchCriteria createSpecification(ComparisonNode cn) {
        
        logger.debug("ComparisonNode:" + cn);
        logger.debug("getArguments:" + cn.getArguments());
        logger.debug("getSelector:" + cn.getSelector());
        logger.debug("getOperator.toString:" + cn.getOperator().toString());
        String argument = cn.getArguments().get(0);
        String selector = cn.getSelector();
        selector = selector.replace("::"," ");
        switch (RsqlSearchOperation.getSimpleOperator(cn.getOperator())) {
            case EQUAL: {
                if (argument instanceof String)
                {
                    SearchCriteria b = new SearchCriteria(selector,argument,SearchCriteria.Operator.EQUAL);
                    return b;
                }
                break;
            }
            case NOT_EQUAL: {
                if (argument instanceof String)
                {
                    SearchCriteria b = new SearchCriteria(selector,argument,SearchCriteria.Operator.NOT_EQUAL);
                    return b;
                }
                break;
            }
            
            case GREATER_THAN: {
                if (argument instanceof String)
                {
                    SearchCriteria b = new SearchCriteria(selector,argument,SearchCriteria.Operator.GREATER_THAN);
                    return b;
                }
                break;
            }
            
            case GREATER_THAN_OR_EQUAL: {
                if (argument instanceof String)
                {
                    SearchCriteria b = new SearchCriteria(selector,argument,SearchCriteria.Operator.GREATER_EQUAL);
                    return b;
                }
                break;
            }
            
            case LESS_THAN: {
                if (argument instanceof String)
                {
                    SearchCriteria b = new SearchCriteria(selector,argument,SearchCriteria.Operator.LESS_THAN);
                    return b;
                }
                break;
            }
            
            case LESS_THAN_OR_EQUAL: {
                if (argument instanceof String)
                {
                    SearchCriteria b = new SearchCriteria(selector,argument,SearchCriteria.Operator.LESS_EQUAL);
                    return b;
                }
                break;
            }
            
            case IN: {
                if (argument instanceof String)
                {
                    SearchCriteria b = new SearchCriteria(selector,argument,SearchCriteria.Operator.IN);
                    return b;
                }
                break;
            }
            
            case NOT_IN: {
                if (argument instanceof String)
                {
                    SearchCriteria b = new SearchCriteria(selector,argument,SearchCriteria.Operator.NOT_IN);
                    return b;
                }
                break;
            }
        }

        return null;
    }
    
}
