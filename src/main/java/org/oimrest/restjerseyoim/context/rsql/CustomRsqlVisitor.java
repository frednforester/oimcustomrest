/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.context.rsql;

import cz.jirutka.rsql.parser.ast.AndNode;
import cz.jirutka.rsql.parser.ast.ComparisonNode;
import cz.jirutka.rsql.parser.ast.OrNode;
import cz.jirutka.rsql.parser.ast.RSQLVisitor;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import org.apache.log4j.Logger;

/**
 *
 * @author Owner
 */
public class CustomRsqlVisitor<T> implements RSQLVisitor<SearchCriteria,Void> {
    
    private static final Logger logger = Logger.getLogger(CustomRsqlVisitor.class.getName());
    SearchCriteriaBuilder<T> builder;

    public CustomRsqlVisitor() {
        builder = new SearchCriteriaBuilder();
    }

    @Override
    public SearchCriteria visit(AndNode an, Void a) {
        logger.debug("AndNode:" + an);
        return builder.createSpecification(an);
        //return an;
    }

    @Override
    public SearchCriteria visit(OrNode ornode, Void a) {
        logger.debug("OrNode:" + ornode);
        return builder.createSpecification(ornode);
        //return ornode;
    }

    @Override
    public SearchCriteria visit(ComparisonNode cn, Void a) {
        logger.debug("ComparisonNode:" + cn);
        logger.debug("ComparisonNode:" + cn.getArguments());
        logger.debug("ComparisonNode:" + cn.getSelector());
        logger.debug("ComparisonNode:" + cn.getOperator().toString());
        return builder.createSpecification(cn);
        //return cn;
    }
    
}
