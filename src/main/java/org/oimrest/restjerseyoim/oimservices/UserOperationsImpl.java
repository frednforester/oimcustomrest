/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.oimservices;

import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.Node;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;
import oracle.iam.exception.OIMServiceException;
import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.OrganizationManagerException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserAlreadyExistsException;
import oracle.iam.identity.exception.UserCreateException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.orgmgmt.api.OrganizationManager;
import oracle.iam.identity.orgmgmt.api.OrganizationManagerConstants;
import oracle.iam.identity.orgmgmt.vo.Organization;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.usermgmt.vo.UserManagerResult;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;
import org.oimrest.restjerseyoim.context.rsql.CustomRsqlVisitor;
import org.oimrest.restjerseyoim.jsontos.OIMAttributeTO;
import org.oimrest.restjerseyoim.jsontos.OperationStatus;
import org.oimrest.restjerseyoim.jsontos.UserTO;
import org.oimrest.restjerseyoim.jsontos.UsersTO;
import org.oimrest.restjerseyoim.oim.IOIMClientWrapper;
import org.oimrest.restjerseyoim.utils.ConverterUtil;
import org.oimrest.restjerseyoim.utils.OIMEntityConverter;

/**
 *
 * @author Owner
 */
@Service
@Named("userOps")
public class UserOperationsImpl implements IUserOperations {

    private static final Logger LOGGER = Logger.getLogger(UserOperationsImpl.class.getName());
    private static final String OIMROLE = "Full-Time";
    private static final String OIMTYPE = "End-User";
    private static final String OIMORG = "Xellerate Users";
    
    @Inject 
    private IOIMClientWrapper oimClientWrapper;
    

    @Override
    public OperationStatus updateUserByKey(String userKey, UserTO userTo) throws NoSuchUserException, UserLookupException, OIMServiceException {

        LOGGER.debug("updateUser:" + userKey);
        LOGGER.debug("updateUser:" + userTo);

        UserManager userManager = (UserManager) this.oimClientWrapper.getClient().getService(UserManager.class);
        String userLogin = null;
        try {
            userLogin = this.getUserLoginByKeyInternal(userKey);
        } catch (Exception e) {
            throw new UserLookupException(e, e.getMessage());
        }
        HashMap userAttrs = new HashMap();
        List<OIMAttributeTO> attributes = userTo.getFields();
        for (OIMAttributeTO attribute : attributes) {
            String name = attribute.getName();
            String value = (String) attribute.getValue();

            userAttrs.put(name, value);
        }
        UserManagerResult res = null;

        User u = new User(userLogin, userAttrs);
        LOGGER.debug("CALLING UPDATE");
        try {
            res = userManager.modify("User Login", userLogin, u);
        } catch (ValidationFailedException ex) {
            LOGGER.error("ValidationFailedException", ex);
            throw new OIMServiceException(ex);
        } catch (NoSuchUserException ex) {
            LOGGER.info("NoSuchUserException:" + userLogin);
            throw ex;
        } catch (UserModifyException ex) {
            LOGGER.error("UserModifyException", ex);
            throw new OIMServiceException(ex);
        } catch (SearchKeyNotUniqueException ex) {
            LOGGER.error("SearchKeyNotUniqueException", ex);
            throw new OIMServiceException(ex);
        }

        OperationStatus status = new OperationStatus();
        status.setStatus(OperationStatus.STATUS_SUCCESS);
        status.setMessage("User Updated");
        return status;
    }

    @Override
    public UserTO getUserLoginByKey(String userKey) throws NoSuchUserException, UserLookupException, OIMServiceException {
        try {
            String userLogin = this.getUserLoginByKeyInternal(userKey);
            UserTO user = new UserTO();
            user.setUserLogin(userLogin);
            return user;
        } catch (Exception e) {
            throw new UserLookupException(e, e.getMessage());
        }
    }

    // new code to support the new rest services in the proper way.
    @Override
    public UserTO getUser(String userLogin) throws SearchKeyNotUniqueException, AccessDeniedException, NoSuchUserException, UserLookupException, OIMServiceException {

        Set<String> retAttrs = new HashSet<String>();
        retAttrs.add(UserManagerConstants.AttributeName.USER_LOGIN.getId());
        retAttrs = null;
        UserManager userManager = (UserManager) this.oimClientWrapper.getClient().getService(UserManager.class);

        try {
            User user = userManager.getDetails(UserManagerConstants.AttributeName.USER_LOGIN.getId(), userLogin, retAttrs);
            UserTO userTo = OIMEntityConverter.getUserTO(user);
            return userTo;
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException", ex);
            throw ex;
        } catch (NoSuchUserException ex) {
            LOGGER.info("NoSuchUserException:" + userLogin);
            throw ex;
        } catch (UserLookupException ex) {
            LOGGER.error("UserLookupException", ex);
            throw ex;
        } catch (SearchKeyNotUniqueException ex) {
            LOGGER.error("SearchKeyNotUniqueException", ex);
            throw ex;
        }

    }

    @Override
    public OperationStatus updateUser(UserTO data) throws NoSuchUserException, UserLookupException, OIMServiceException {

        if (data == null) {
            throw new NoSuchUserException("ERROR User Key or User ID missing from input", "ERROR User Key or User ID missing from input");
        }
        String userKey = data.getId();
        if (userKey != null) {
            return this.updateUserByKey(userKey, data);
        }
        String userLogin = data.getUserLogin();
        if (userLogin != null) {
            UserManager userManager = (UserManager) this.oimClientWrapper.getClient().getService(UserManager.class);
            try {
                Map attrMap = ConverterUtil.getAttrMapFromList(data.getFields());
                UserManagerResult res = null;
                HashMap newMap = new HashMap();
                newMap.putAll(attrMap);
                User u = new User(userLogin, newMap);
                res = userManager.modify("User Login", userLogin, u);

                OperationStatus status = new OperationStatus();
                status.setStatus(OperationStatus.STATUS_SUCCESS);
                status.setMessage("User Updated");
                return status;
            } catch (ValidationFailedException ex) {
                LOGGER.error("ValidationFailedException:" + ex.getMessage(),ex);
                throw ex;
            } catch (AccessDeniedException ex) {
                LOGGER.error("AccessDeniedException:" + ex.getMessage(),ex);
                throw ex;
            } catch (UserModifyException ex) {
                LOGGER.error("UserModifyException:" + ex.getMessage(),ex);
                throw ex;
            } catch (NoSuchUserException ex) {
                LOGGER.error("NoSuchUserException:" + ex.getMessage(),ex);
                throw ex;
            } catch (SearchKeyNotUniqueException ex) {
                LOGGER.error("SearchKeyNotUniqueException:" + ex.getMessage(),ex);
                throw ex;
            } catch (Exception ex)
            {
                LOGGER.error("Exception ValidationFailedException:" + ex.getMessage(),ex);
                throw new ValidationFailedException(ex,"Exception ValidationFailedException:" + ex.getMessage());
            }
        }
        OperationStatus status = new OperationStatus();
        status.setStatus(OperationStatus.STATUS_FAILURE);
        status.setMessage("Unknown Error");
        return status;
    }
    @Override
    public UserTO getUserByKey(String userKey) throws SearchKeyNotUniqueException, AccessDeniedException, NoSuchUserException, UserLookupException, OIMServiceException {

        UserManager userManager = (UserManager) this.oimClientWrapper.getClient().getService(UserManager.class);
        try {
            User user = userManager.getDetails(UserManagerConstants.AttributeName.USER_KEY.getId(), userKey, null);
            UserTO userTo = OIMEntityConverter.getUserTO(user);
            return userTo;
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException", ex);
            throw ex;
        } catch (NoSuchUserException ex) {
            LOGGER.info("NoSuchUserException:" + userKey);
            throw ex;
        } catch (UserLookupException ex) {
            LOGGER.error("UserLookupException", ex);
            throw ex;
        } catch (SearchKeyNotUniqueException ex) {
            LOGGER.error("SearchKeyNotUniqueException", ex);
            throw ex;
        }
    }

    private String getUserLoginByKeyInternal(String userKey) throws Exception {

        Set<String> retAttrs = new HashSet<String>();
        retAttrs.add(UserManagerConstants.AttributeName.USER_LOGIN.getId());

        UserManager userManager = (UserManager) this.oimClientWrapper.getClient().getService(UserManager.class);

        try {
            User user = userManager.getDetails(UserManagerConstants.AttributeName.USER_KEY.getId(), userKey, retAttrs);
            return user.getLogin();
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException", ex);
            throw new Exception(ex);
        } catch (NoSuchUserException ex) {
            LOGGER.info("NoSuchUserException:" + userKey);
            throw new Exception(ex);
        } catch (UserLookupException ex) {
            LOGGER.error("UserLookupException", ex);
            throw new Exception(ex);
        } catch (SearchKeyNotUniqueException ex) {
            LOGGER.error("SearchKeyNotUniqueException", ex);
            throw new Exception(ex);
        }

    }

    private String getSimpleName(Serializable value) {
        if (value.getClass().getSimpleName().equals("char[]")) {
            return "Clob";
        }
        return value.getClass().getSimpleName();
    }

    @Override
    public UsersTO searchUsers(Object searchQuery) throws NoSuchUserException, UserLookupException, OIMServiceException {

        UserManager userManager = (UserManager) this.oimClientWrapper.getClient().getService(UserManager.class);
        Node rootNode = new RSQLParser().parse(searchQuery.toString());
        SearchCriteria spec = rootNode.accept(new CustomRsqlVisitor<SearchCriteria>());

        if (spec == null) {
            LOGGER.error("query parsing error::" + searchQuery);
            throw new OIMServiceException("query parsing error::" + searchQuery);
        }
        LOGGER.debug("SearchCriteria:" + spec);
        HashMap<String, Object> config = new HashMap<String, Object>();
        config.put("STARTROW", "0");
        config.put("ENDROW", Integer.toString(Integer.MAX_VALUE));
        List<UserTO> jsonUsers = new ArrayList();
        try {
            List<User> users = userManager.search(spec, null, config);
            LOGGER.debug("Got Users:" + users.size());
            if (users.size() > 50) {
                LOGGER.error("Too Many Users Returned. Please refine you search");
                throw new OIMServiceException("Too Many Users Returned. Please refine you search");
            }
            LOGGER.debug("Iterate users" + users.size());
            for (User u : users) {
                LOGGER.debug("Iterate");
                UserTO to = OIMEntityConverter.getUserTO(u);
                LOGGER.debug("UserTo:" + to);
                jsonUsers.add(to);

            }

        } catch (Exception e) {
            LOGGER.error("APIError:" + e.toString(), e);
            throw new OIMServiceException(e);
        }
        UsersTO users = new UsersTO();
        users.setUsers(jsonUsers);
        return users;
    }

    @Override
    public UserTO createUser(UserTO data) throws ValidationFailedException,AccessDeniedException,UserAlreadyExistsException,UserCreateException,OIMServiceException 
    {
        UserManager userManager = (UserManager) this.oimClientWrapper.getClient().getService(UserManager.class);
        HashMap<String, Object> createAttributes = new HashMap<String, Object>();
		String userKey = null;
        List<OIMAttributeTO> attrs = data.getFields();
        Map extraAttrs = ConverterUtil.getAttrMapFromList(attrs);
        
        if (data.getOimType() == null)
        {
            data.setOimType(UserOperationsImpl.OIMTYPE);
        }
        if (data.getOimRole()== null)
        {
            data.setOimType(UserOperationsImpl.OIMROLE);
        }
        if (data.getOrgKey() == null)
        {
            Long orgKey = this.getOrganizationKey(UserOperationsImpl.OIMORG);
            //createAttributes.put("act_key", orgKey);
            //createAttributes.put("Organization",orgKey);
            data.setOrgKey(orgKey.toString());
        }
        createAttributes.putAll(extraAttrs);
		createAttributes.put("User Login", data.getUserLogin());
		createAttributes.put("First Name",data.getFirstName());
		createAttributes.put("Last Name", data.getLastName());
        createAttributes.put("act_key",Long.valueOf(data.getOrgKey()));
		//createAttributes.put(oracle.iam.identity.utils.Constants.PASSWORD, "Welcome1");
		createAttributes.put("Xellerate Type", data.getOimType());
		createAttributes.put("Role", data.getOimRole());
        LOGGER.debug("Submitting Create:" + createAttributes);
        
        try
        {
            UserManagerResult userResult = userManager.create(new User(null,createAttributes));
            userKey = userResult.getEntityId();
            UserTO thisUser = this.getUserByKey(userKey);
            LOGGER.debug("New User:" + thisUser);
            //data.setId(userKey);
            return thisUser;
        }
        catch(ValidationFailedException e)
        {
            LOGGER.error("ValidationFailedException:" + e.toString(), e);
            throw e;
        }
        catch(AccessDeniedException e)
        {
            LOGGER.error("AccessDeniedException:" + e.toString(), e);
            throw e;
        }
        catch(UserAlreadyExistsException e)
        {
            LOGGER.error("UserAlreadyExistsException:" + e.toString(), e);
            throw e;
        }
        catch(UserCreateException e)
        {
            LOGGER.error("UserCreateException:" + e.toString(), e);
            throw e;
        }
        catch(Exception e)
        {
            LOGGER.error("APIError:" + e.toString(), e);
            throw new OIMServiceException(e);
        }
    }
    
    /**
     * return a single org object
     * @param name or key
     * @param isOrgName
     * @return
     * @throws OIMHelperException 
     */
    private Long getOrganizationKey(String name) throws OrganizationManagerException,AccessDeniedException
    {
        Set<String> retAttrs = new HashSet<String>();
        retAttrs.add(OrganizationManagerConstants.AttributeName.ID_FIELD.getId());
        retAttrs.add(OrganizationManagerConstants.AttributeName.ORG_PARENT_KEY.getId());
        retAttrs.add(OrganizationManagerConstants.AttributeName.ORG_NAME.getId());
        retAttrs.add(OrganizationManagerConstants.AttributeName.ORG_STATUS.getId());
        retAttrs.add(OrganizationManagerConstants.AttributeName.ORG_TYPE.getId());
        retAttrs.add(OrganizationManagerConstants.AttributeName.ORG_PARENT_NAME.getId());
        try
        {
            OrganizationManager orgOp = (OrganizationManager) this.oimClientWrapper.getClient().getService(OrganizationManager.class);
            Organization org = orgOp.getDetails(name, retAttrs, true);
            if (org != null)
            {
                String key = org.getEntityId();
                Long keyL = Long.valueOf(key);
                return keyL;
            }
        }
        catch(OrganizationManagerException e)
        {
            LOGGER.error("OrganizationManagerException:" + e.getMessage(),e);
            throw e;
        }
        catch(AccessDeniedException e)
        {
            LOGGER.error("AccessDeniedException:" + e.getMessage(),e);
            throw e;
        }
        return null;
    }


}
