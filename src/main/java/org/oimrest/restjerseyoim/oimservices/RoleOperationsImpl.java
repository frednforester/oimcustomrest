/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.oimservices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import javax.inject.Inject;
import javax.inject.Named;
import oracle.iam.identity.exception.NoSuchRoleException;
import oracle.iam.identity.exception.RoleAlreadyExistsException;
import oracle.iam.identity.exception.RoleCategorySearchException;
import oracle.iam.identity.exception.RoleCreateException;
import oracle.iam.identity.exception.RoleDeleteException;
import oracle.iam.identity.exception.RoleLookupException;
import oracle.iam.identity.exception.RoleModifyException;
import oracle.iam.identity.exception.RoleSearchException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.rolemgmt.api.RoleCategoryManager;
import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.rolemgmt.api.RoleManagerConstants;
import oracle.iam.identity.rolemgmt.vo.Role;
import oracle.iam.identity.rolemgmt.vo.RoleCategory;
import oracle.iam.identity.rolemgmt.vo.RoleManagerResult;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.requestprofile.vo.RequestProfileConstants;
import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;
import org.oimrest.restjerseyoim.jsontos.OperationStatus;
import org.oimrest.restjerseyoim.jsontos.RoleTO;
import org.oimrest.restjerseyoim.jsontos.RolesTO;
import org.oimrest.restjerseyoim.oim.IOIMClientWrapper;
import org.oimrest.restjerseyoim.utils.ConverterUtil;
import org.oimrest.restjerseyoim.utils.OIMEntityConverter;

/**
 *
 * @author Owner
 */
@Service
@Named("roleOps")
public class RoleOperationsImpl implements IRoleOperations {

    private static final Logger LOGGER = Logger.getLogger(RoleOperationsImpl.class.getName());

    @Inject
    private IOIMClientWrapper oimClientWrapper;

    @Override
    public RoleTO getRole(String roleName) throws SearchKeyNotUniqueException,AccessDeniedException,NoSuchRoleException,RoleLookupException,Exception {
        LOGGER.debug("getRole:" + roleName);
        try {
            Role role = this.getRoleByNameInternal(roleName);
            LOGGER.debug("gotrole:" + role.getName());
            LOGGER.debug("gotrole:" + role.getAttributes());
            RoleTO roleTo = OIMEntityConverter.getRoleTO(role);
            roleTo.setName(role.getName());
            roleTo.setDisplayName(role.getDisplayName());
            roleTo.setId(role.getEntityId());
            return roleTo;
        } catch (SearchKeyNotUniqueException e) {
            LOGGER.error("SearchKeyNotUniqueException:" + e.getMessage(), e);
            throw e;
        } catch (AccessDeniedException e) {
            LOGGER.error("AccessDeniedException:" + e.getMessage(), e);
            throw e;
        } catch (NoSuchRoleException e) {
            LOGGER.error("NoSuchRoleException:" + e.getMessage(), e);
            throw e;
        } catch (RoleLookupException e) {
            LOGGER.error("RoleLookupException:" + e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            LOGGER.error("Exception: " + e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public RolesTO getAllRoles(String searchArg) throws AccessDeniedException, RoleSearchException {
        RolesTO rolesTo = new RolesTO();
        List<RoleTO> rolesList = new ArrayList();
        RoleManager roleManager = (RoleManager) this.oimClientWrapper.getClient().getService(RoleManager.class);
        SearchCriteria criteria = new SearchCriteria(RoleManagerConstants.RoleAttributeName.NAME.getId(), searchArg, SearchCriteria.Operator.EQUAL);
        Set<String> retAttrs = new HashSet<String>();
        retAttrs.add(RoleManagerConstants.RoleAttributeName.CATEGORY_KEY.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.CREATE_DATE.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.DATA_LEVEL.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.DESCRIPTION.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.DISPLAY_NAME.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.EMAIL.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.KEY.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.LDAP_DN.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.LDAP_GUID.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.NAME.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.NAMESPACE.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.OWNER_KEY.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.UNIQUE_NAME.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.UPDATE_DATE.getId());
        retAttrs.add(RoleManagerConstants.RoleAttributeName.UPDATED_BY.getId());
        Map configParms = new HashMap();
        configParms.put(RequestProfileConstants.SEARCH_STARTROW, "0");
        configParms.put(RequestProfileConstants.SEARCH_ENDROW, new Integer(Integer.MAX_VALUE).toString());

        try {
            List<Role> roleList = roleManager.search(criteria, null, configParms);
            for (Role role : roleList) {
                RoleTO roleTo = OIMEntityConverter.getRoleTO(role);
                rolesList.add(roleTo);
            }
            rolesTo.setRoles(rolesList);
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException", ex);
            throw ex;
        } catch (RoleSearchException ex) {
            LOGGER.error("RoleSearchException", ex);
            throw ex;
        }
        return rolesTo;
    }

    @Override
    public OperationStatus updateByName(RoleTO roleTo) throws ValidationFailedException, AccessDeniedException, Exception {
        Role newRole = null;
        LOGGER.debug("updateByName:" + roleTo);
        try {
            RoleManager roleOp = (RoleManager) this.oimClientWrapper.getClient().getService(RoleManager.class);
            RoleTO oldRole = this.getRole(roleTo.getName());
            LOGGER.debug("Found Role for update:" + oldRole);
            newRole = new Role(oldRole.getId());
            newRole.setName(oldRole.getName());
            Map fieldMap = ConverterUtil.getAttrMapFromList(roleTo.getFields());
            if (fieldMap != null && fieldMap.size() > 0) {
                Set<String> keySet = fieldMap.keySet();
                for (String key : keySet) {
                    newRole.setAttribute(key, fieldMap.get(key));
                }
            }
            RoleManagerResult res = roleOp.modify(RoleManagerConstants.RoleAttributeName.NAME.getId(), newRole.getName(), newRole);
            Map resMap = res.getFailedResults();
            if (resMap != null && !resMap.isEmpty()) {
                LOGGER.error(res.getFailedResults());
                throw new Exception("Failed to update role " + roleTo.getName());
            }
            OperationStatus status = new OperationStatus();
            status.setStatus(OperationStatus.STATUS_SUCCESS);
            status.setMessage("Role Updated");
            return status;
        } catch (ValidationFailedException ex) {
            LOGGER.error("ValidationFailedException", ex);
            throw ex;
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException", ex);
            throw ex;
        } catch (RoleModifyException ex) {
            LOGGER.error("RoleModifyException", ex);
            throw new Exception(ex);
        } catch (NoSuchRoleException ex) {
            LOGGER.error("NoSuchRoleException", ex);
            throw new Exception(ex);
        } catch (SearchKeyNotUniqueException ex) {
            LOGGER.error("SearchKeyNotUniqueException", ex);
            throw new Exception(ex);
        } catch (RoleLookupException ex) {
            LOGGER.error("RoleLookupException", ex);
            throw new Exception(ex);
        }
    }
    
    public OperationStatus createRole(RoleTO roleTo) throws ValidationFailedException,AccessDeniedException,RoleAlreadyExistsException,RoleCreateException
    {
        RoleManager roleOp = (RoleManager) this.oimClientWrapper.getClient().getService(RoleManager.class);
        RoleCategory roleCat = null;
        OperationStatus status = new OperationStatus();
        
        try
        {
            roleCat = this.getCategoryByNameInternal(roleTo.getCategoryName());
            if (roleCat == null)
            {
                throw new RoleCreateException("Role Catagory not found:" + roleTo.getCategoryName());
            }
        }
        catch(Exception e)
        {
            LOGGER.error("API ERROR:" + e.getMessage(),e);
            throw new RoleCreateException("API ERROR:" + roleTo.getCategoryName(),e);
        }
        try 
        {
            RoleManagerResult roleResult = null; 
            String roleName = roleTo.getName();
            String dispName = roleTo.getDisplayName();
            HashMap creationAttributes = new HashMap();     
            creationAttributes.put(RoleManagerConstants.ROLE_NAME, roleName); 
            creationAttributes.put(RoleManagerConstants.ROLE_DISPLAY_NAME, dispName); 
            creationAttributes.put(RoleManagerConstants.ROLE_DESCRIPTION, dispName + "Description"); 
            creationAttributes.put(RoleManagerConstants.ROLE_CATEGORY_KEY, Long.parseLong(roleCat.getEntityId())); 

            Role role = new Role(creationAttributes); 
            roleResult = roleOp.create(role);
            String entityId = roleResult.getEntityId();
            status.setStatus(OperationStatus.STATUS_SUCCESS);
            status.setMessage("Role Created:" + roleResult.getSucceededResults());
            return status;
            
        } catch (ValidationFailedException ex) {
            LOGGER.error("ValidationFailedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (RoleAlreadyExistsException ex) {
            LOGGER.error("RoleAlreadyExistsException:" + ex.getMessage(),ex);
            throw ex;
        } catch (RoleCreateException ex) {
            LOGGER.error("RoleCreateException:" + ex.getMessage(),ex);
            throw ex;
        }
    }
    
    public OperationStatus deleteRole(String roleName) throws ValidationFailedException,AccessDeniedException,RoleDeleteException,NoSuchRoleException
    {
        RoleManager roleOp = (RoleManager) this.oimClientWrapper.getClient().getService(RoleManager.class);
        OperationStatus status = new OperationStatus();
        RoleManagerResult res = null;
        try {
            Role role = this.getRoleByNameInternal(roleName);
            if (role != null && role.getEntityId() != null)
            {
                res = roleOp.delete(role.getEntityId());
                
            }
            if (res == null)
            {
                res = new RoleManagerResult("Unknown Status");
            }
            status.setMessage(res.getStatus());
            status.setStatus(res.getStatus());
            return status;
        } catch (ValidationFailedException ex) {
            LOGGER.error("ValidationFailedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (RoleDeleteException ex) {
            LOGGER.error("RoleDeleteException:" + ex.getMessage(),ex);
            throw ex;
        } catch (NoSuchRoleException ex) {
            LOGGER.error("NoSuchRoleException:" + ex.getMessage(),ex);
            throw ex;
        } catch (Exception ex) {
            LOGGER.error("Exception:" + ex.getMessage(),ex);
            throw new ValidationFailedException(ex,"API Error:" + ex.getMessage());
        }
        
    }
    
    private Role getRoleByNameInternal(String roleName) throws SearchKeyNotUniqueException,AccessDeniedException,NoSuchRoleException,RoleLookupException
    {
        RoleManager roleOp = (RoleManager) this.oimClientWrapper.getClient().getService(RoleManager.class);
        try {
            Role role = roleOp.getDetails(RoleManagerConstants.ROLE_NAME, roleName,null);
            return role;
        } catch (SearchKeyNotUniqueException ex) {
            LOGGER.error("SearchKeyNotUniqueException:" + ex.getMessage(),ex);
            throw ex;
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (NoSuchRoleException ex) {
            LOGGER.error("NoSuchRoleException:" + ex.getMessage(),ex);
            throw ex;
        } catch (RoleLookupException ex) {
            LOGGER.error("RoleLookupException:" + ex.getMessage(),ex);
            throw ex;
        }
    }
    
    private RoleCategory getCategoryByNameInternal(String categoryName) throws AccessDeniedException,RoleCategorySearchException
    {
        RoleCategoryManager  roleCatOp = (RoleCategoryManager ) this.oimClientWrapper.getClient().getService(RoleCategoryManager .class);
        SearchCriteria criteria = new SearchCriteria(RoleManagerConstants.ROLE_CATEGORY_NAME, categoryName, 
        SearchCriteria.Operator.EQUAL);  
        Set retSet = new HashSet(); 
        retSet.add(RoleManagerConstants.ROLE_CATEGORY_NAME); 
        try { 
            List<RoleCategory> categories = roleCatOp.search(criteria, retSet, null);
            if (categories != null && categories.size() > 0)
            {
                RoleCategory roleCat = categories.get(0);
                return roleCat;
            }
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (RoleCategorySearchException ex) {
            LOGGER.error("RoleCategorySearchException:" + ex.getMessage(),ex);
            throw ex;
        }
        return null;
    }

}
