/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.oimservices;

import oracle.iam.identity.exception.RoleGrantException;
import oracle.iam.identity.exception.RoleGrantRevokeException;
import oracle.iam.identity.exception.RoleMemberException;
import oracle.iam.identity.exception.UserMembershipException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import org.glassfish.jersey.spi.Contract;
import org.oimrest.restjerseyoim.jsontos.OperationStatus;
import org.oimrest.restjerseyoim.jsontos.RoleMembershipTO;

/**
 *
 * @author Owner
 */
@Contract
public interface IMembershipOperations {
    
    OperationStatus addRoleMember(RoleMembershipTO membershipTo) throws ValidationFailedException,AccessDeniedException,RoleGrantException;
    RoleMembershipTO getRoleMembers(String roleName) throws ValidationFailedException,AccessDeniedException,RoleMemberException;
    RoleMembershipTO getMembersRoles(String userLogin) throws AccessDeniedException,UserMembershipException,ValidationFailedException;
    OperationStatus deleteRoleMember(RoleMembershipTO membershipTo) throws ValidationFailedException,AccessDeniedException,UserMembershipException,RoleGrantRevokeException;
    
    
}
