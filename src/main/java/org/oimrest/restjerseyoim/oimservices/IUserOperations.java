/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.oimservices;

import oracle.iam.exception.OIMServiceException;
import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserAlreadyExistsException;
import oracle.iam.identity.exception.UserCreateException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import org.glassfish.jersey.spi.Contract;
import org.oimrest.restjerseyoim.jsontos.OperationStatus;
import org.oimrest.restjerseyoim.jsontos.UserTO;
import org.oimrest.restjerseyoim.jsontos.UsersTO;

/**
 *
 * @author Owner
 */
@Contract
public interface IUserOperations {

    UserTO getUserLoginByKey(String userKey) throws NoSuchUserException, UserLookupException, OIMServiceException;
    OperationStatus updateUserByKey(String userKey, UserTO data) throws NoSuchUserException, UserLookupException, OIMServiceException;
    UserTO getUser(String userLogin) throws SearchKeyNotUniqueException,AccessDeniedException,NoSuchUserException, UserLookupException, OIMServiceException;
    UserTO getUserByKey(String userKey) throws SearchKeyNotUniqueException,AccessDeniedException,NoSuchUserException, UserLookupException, OIMServiceException;
    OperationStatus updateUser(UserTO data) throws NoSuchUserException, UserLookupException, OIMServiceException;
    UsersTO searchUsers(Object searchQuery) throws NoSuchUserException, UserLookupException, OIMServiceException;
    UserTO createUser(UserTO data) throws ValidationFailedException,AccessDeniedException,UserAlreadyExistsException,UserCreateException,OIMServiceException;
}
