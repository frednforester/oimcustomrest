/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.oimservices;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import javax.inject.Inject;
import javax.inject.Named;
import oracle.iam.identity.exception.NoSuchRoleException;
import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.RoleGrantException;
import oracle.iam.identity.exception.RoleGrantRevokeException;
import oracle.iam.identity.exception.RoleLookupException;
import oracle.iam.identity.exception.RoleMemberException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserMembershipException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.rolemgmt.api.RoleManagerConstants;
import oracle.iam.identity.rolemgmt.vo.Role;
import oracle.iam.identity.rolemgmt.vo.RoleManagerResult;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;
import org.oimrest.restjerseyoim.jsontos.OperationStatus;
import org.oimrest.restjerseyoim.jsontos.RoleMembershipTO;
import org.oimrest.restjerseyoim.oim.IOIMClientWrapper;

/**
 *
 * @author Owner
 */
@Service
@Named("memberOps")
public class MembershipOperationsImpl implements IMembershipOperations {
    
    @Inject
    private IOIMClientWrapper oimClientWrapper;
    
    private static final Logger LOGGER = Logger.getLogger(MembershipOperationsImpl.class.getName());

    @Override
    public OperationStatus addRoleMember(RoleMembershipTO membershipTo) throws ValidationFailedException,AccessDeniedException,RoleGrantException{
        LOGGER.debug("addRoleMember:" + membershipTo);
        RoleManager roleManager = (RoleManager) this.oimClientWrapper.getClient().getService(RoleManager.class);
        
        String usrLogin = membershipTo.getRoleMember();
        String roleName = membershipTo.getRoleName();
        String usrKey = null;
        String roleKey = null;
        try
        {
            usrKey = this.getUserKeyByLoginInternal(usrLogin);
            Role role = this.getRoleByNameInternal(roleName);
            roleKey = role.getEntityId();
        }
        catch(Exception e)
        {
            LOGGER.error("Error validating rolename or userLogin",e);
            throw new ValidationFailedException("Error validating rolename or userLogin",e.getMessage());
        }
        
        Set userSet = new HashSet();
        userSet.add(usrKey);
        try {
            RoleManagerResult res = roleManager.grantRole(roleKey, userSet);
            OperationStatus status = new OperationStatus();
            status.setMessage(res.getStatus());
            status.setStatus(res.getStatus());
            return status;
        } catch (ValidationFailedException ex) {
            LOGGER.error("ValidationFailedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (RoleGrantException ex) {
            LOGGER.error("RoleGrantException:" + ex.getMessage(),ex);
            throw ex;
        }
    }

    @Override
    public RoleMembershipTO getRoleMembers(String roleName) throws ValidationFailedException,AccessDeniedException,RoleMemberException{
        LOGGER.debug("addRoleMember:" + roleName);
        RoleManager roleManager = (RoleManager) this.oimClientWrapper.getClient().getService(RoleManager.class);
        Role role = null;
        try
        {
            role = this.getRoleByNameInternal(roleName);
        }
        catch(Exception e)
        {
            LOGGER.error("Error validating userLogin",e);
            throw new ValidationFailedException("Error validating userLogin",e.getMessage());
        }
        
        try {
            List users = roleManager.getRoleMembers(roleName, true);
            RoleMembershipTO membershipTo = new RoleMembershipTO();
            membershipTo.setRoleMembers(users);
            return membershipTo;
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (RoleMemberException ex) {
            LOGGER.error("RoleMemberException:" + ex.getMessage(),ex);
            throw ex;
        }
    }

    @Override
    public RoleMembershipTO getMembersRoles(String userLogin) throws AccessDeniedException,UserMembershipException,ValidationFailedException {
        LOGGER.debug("getMembersRoles:" + userLogin);
        String userKey = null;
        RoleManager roleManager = (RoleManager) this.oimClientWrapper.getClient().getService(RoleManager.class);
        try
        {
            userKey = this.getUserKeyByLoginInternal(userLogin);
        }
        catch(Exception e)
        {
            LOGGER.error("Error validating userLogin",e);
            throw new ValidationFailedException("Error validating userLogin",e.getMessage());
        }
        
        try {
            List userRoles = roleManager.getUserMemberships(userKey,false);
            RoleMembershipTO membershipTo = new RoleMembershipTO();
            membershipTo.setMemberRoles(userRoles);
            return membershipTo;
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (UserMembershipException ex) {
            LOGGER.error("UserMembershipException:" + ex.getMessage(),ex);
            throw ex;
        }
        
    }

    @Override
    public OperationStatus deleteRoleMember(RoleMembershipTO membershipTo) throws ValidationFailedException,AccessDeniedException,UserMembershipException,RoleGrantRevokeException  {
        LOGGER.debug("deleteRoleMember:" + membershipTo);
        OperationStatus status = new OperationStatus();
        RoleManager roleManager = (RoleManager) this.oimClientWrapper.getClient().getService(RoleManager.class);
        String usrLogin = membershipTo.getRoleMember();
        String roleName = membershipTo.getRoleName();
        String usrKey = null;
        String roleKey = null;
        try
        {
            usrKey = this.getUserKeyByLoginInternal(usrLogin);
            Role role = this.getRoleByNameInternal(roleName);
            roleKey = role.getEntityId();
        }
        catch(Exception e)
        {
            LOGGER.error("Error validating rolename or userLogin",e);
            //throw new ValidationFailedException("Error validating rolename or userLogin",e.getMessage());
        }
        Set roleKeys = new HashSet();
        
        List<Role> usrRoles;
        try {
            usrRoles = roleManager.getUserMemberships(String.valueOf(usrKey), true);
            int size = usrRoles.size();
            for(Role aRole : usrRoles)
            {
                if (aRole.getName().equalsIgnoreCase(roleName))
                {
                    roleKeys.add(aRole.getEntityId());
                }
            }
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (UserMembershipException ex) {
            LOGGER.error("UserMembershipException:" + ex.getMessage(),ex);
            throw ex;
        }
        
        if (roleKeys.isEmpty())
        {
            status.setStatus(OperationStatus.STATUS_SUCCESS);
            status.setMessage("User Does Not have role:" + usrLogin + ":" + roleName);
            return new OperationStatus();
        }
        
        try {
            RoleManagerResult removeResult = roleManager.revokeRoleGrants(usrKey, roleKeys);
            status.setStatus(OperationStatus.STATUS_SUCCESS);
            status.setMessage("User:" + usrLogin + " Removed from role:" + roleName);
            return new OperationStatus();
        } catch (ValidationFailedException ex) {
            LOGGER.error("ValidationFailedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (RoleGrantRevokeException ex) {
            LOGGER.error("RoleGrantRevokeException:" + ex.getMessage(),ex);
            throw ex;
        }
    }
    
    private Role getRoleByNameInternal(String roleName) throws Exception
    {
        RoleManager roleOp = (RoleManager) this.oimClientWrapper.getClient().getService(RoleManager.class);
        try {
            Role role = roleOp.getDetails(RoleManagerConstants.ROLE_NAME, roleName,null);
            return role;
        } catch (SearchKeyNotUniqueException ex) {
            LOGGER.error("SearchKeyNotUniqueException:" + ex.getMessage(),ex);
            throw ex;
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException:" + ex.getMessage(),ex);
            throw ex;
        } catch (NoSuchRoleException ex) {
            LOGGER.error("NoSuchRoleException:" + ex.getMessage(),ex);
            throw ex;
        } catch (RoleLookupException ex) {
            LOGGER.error("RoleLookupException:" + ex.getMessage(),ex);
            throw ex;
        }
    }
    
    private String getUserKeyByLoginInternal(String userLogin) throws Exception {

        Set<String> retAttrs = new HashSet<String>();
        retAttrs.add(UserManagerConstants.AttributeName.USER_LOGIN.getId());

        UserManager userManager = (UserManager) this.oimClientWrapper.getClient().getService(UserManager.class);

        try {
            User user = userManager.getDetails(UserManagerConstants.AttributeName.USER_LOGIN.getId(), userLogin, retAttrs);
            return user.getId();
        } catch (AccessDeniedException ex) {
            LOGGER.error("AccessDeniedException", ex);
            throw new Exception(ex);
        } catch (NoSuchUserException ex) {
            LOGGER.info("NoSuchUserException:" + userLogin);
            throw new Exception(ex);
        } catch (UserLookupException ex) {
            LOGGER.error("UserLookupException", ex);
            throw new Exception(ex);
        } catch (SearchKeyNotUniqueException ex) {
            LOGGER.error("SearchKeyNotUniqueException", ex);
            throw new Exception(ex);
        }

    }
    
}
