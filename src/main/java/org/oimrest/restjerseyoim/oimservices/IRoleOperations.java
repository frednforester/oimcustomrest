/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.oimservices;

import oracle.iam.identity.exception.NoSuchRoleException;
import oracle.iam.identity.exception.RoleAlreadyExistsException;
import oracle.iam.identity.exception.RoleCreateException;
import oracle.iam.identity.exception.RoleDeleteException;
import oracle.iam.identity.exception.RoleLookupException;
import oracle.iam.identity.exception.RoleSearchException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import org.oimrest.restjerseyoim.jsontos.OperationStatus;
import org.oimrest.restjerseyoim.jsontos.RoleTO;
import org.oimrest.restjerseyoim.jsontos.RolesTO;


/**
 *
 * @author Owner
 */
public interface IRoleOperations {
    
    RoleTO getRole(String roleName) throws SearchKeyNotUniqueException,AccessDeniedException,NoSuchRoleException,RoleLookupException,Exception;
    RolesTO getAllRoles(String searchArg) throws AccessDeniedException,RoleSearchException;
    OperationStatus updateByName(RoleTO roleTo) throws ValidationFailedException,AccessDeniedException,Exception;
    OperationStatus createRole(RoleTO roleTo) throws ValidationFailedException,AccessDeniedException,RoleAlreadyExistsException,RoleCreateException;
    OperationStatus deleteRole(String roleName) throws ValidationFailedException,AccessDeniedException,RoleDeleteException,NoSuchRoleException;
}
