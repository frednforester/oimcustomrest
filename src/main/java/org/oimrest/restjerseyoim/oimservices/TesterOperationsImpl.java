/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.oimservices;

import javax.inject.Inject;
import oracle.iam.platform.OIMClient;
import org.jvnet.hk2.annotations.Service;
import org.oimrest.restjerseyoim.oim.IOIMClientWrapper;

/**
 *
 * @author Owner
 */
@Service
public class TesterOperationsImpl implements ITesterOperations {

    
    @Inject 
    private IOIMClientWrapper oimClientWrapper;
    
    @Override
    public String testMe() {
        
        OIMClient client = this.oimClientWrapper.getClient();
        return "SUCCESS";
    }
    
}
