/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.oimservices;

import javax.inject.Inject;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;
import org.oimrest.restjerseyoim.oim.IOIMClientWrapper;

/**
 *
 * @author Owner
 */
@Service
public class PasswordOperationsImpl implements IPasswordOperations {
    
    private static final Logger LOGGER = Logger.getLogger(PasswordOperationsImpl.class.getName());
    
    @Inject 
    private IOIMClientWrapper oimClientWrapper;
    
    @Override
    public void changeUserPassword(String userLogin, String newPassword) throws AccessDeniedException, UserManagerException {
        
        UserManager userManager = (UserManager) this.oimClientWrapper.getClient().getService(UserManager.class);
        userManager.changePassword(userLogin, newPassword.toCharArray(), true, null, false, false);
    }
    
}
