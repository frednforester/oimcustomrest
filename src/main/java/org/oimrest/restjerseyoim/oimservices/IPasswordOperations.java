/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.oimservices;

import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import org.glassfish.jersey.spi.Contract;

/**
 *
 * @author Owner
 */
@Contract
public interface IPasswordOperations {
    
    void changeUserPassword(String userLogin, String newPassword) throws AccessDeniedException,UserManagerException;
    
}
