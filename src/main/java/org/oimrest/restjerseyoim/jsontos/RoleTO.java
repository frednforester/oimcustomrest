/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.jsontos;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessOrder;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Owner
 */

@XmlRootElement(name = "role")
@XmlAccessorOrder(XmlAccessOrder.ALPHABETICAL)
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleTO {

    private String id;
    private String displayName;
    private String ownerlogin;
    private String rolekey;
    private String name;
    private String categoryName;
    
    @XmlElementWrapper(name = "fields")
    @XmlElement(name = "field")
    private List<OIMAttributeTO> fields = new ArrayList();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getOwnerlogin() {
        return ownerlogin;
    }

    public void setOwnerlogin(String ownerlogin) {
        this.ownerlogin = ownerlogin;
    }

    public String getRolekey() {
        return rolekey;
    }

    public void setRolekey(String rolekey) {
        this.rolekey = rolekey;
    }

    public List<OIMAttributeTO> getFields() {
        return fields;
    }

    public void setFields(List<OIMAttributeTO> fields) {
        this.fields = fields;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "RoleTO{" + "id=" + id + ", displayName=" + displayName + ", ownerlogin=" + ownerlogin + ", rolekey=" + rolekey + ", name=" + name + ", categoryName=" + categoryName + ", fields=" + fields + '}';
    }
    
}
