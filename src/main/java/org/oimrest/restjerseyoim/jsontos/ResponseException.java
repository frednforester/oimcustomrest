/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.jsontos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Owner
 */
public class ResponseException extends RuntimeException {
    
    private List<ErrorInfo> errors;

    public List<ErrorInfo> getErrors() {
        return this.errors;
    }

    public ResponseException(List<ErrorInfo> errors) {
        super();
        this.errors = errors;
    }

    public ResponseException(String message, List<ErrorInfo> errors) {
        super(message);
        this.errors = errors;
    }
    
    public ResponseException(String message, ErrorInfo error) {
        super(message);
        errors = new ArrayList();
        errors.add(error);
        this.errors = errors;
    }
    
}
