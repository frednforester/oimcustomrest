/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.jsontos;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;





@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class OIMAttributeTO
{
  @XmlElement(name = "name")
  private String name;
  @XmlElement(name = "value")
  private Object value;
  
  public OIMAttributeTO(String name, Object value) {
    this.name = name;
    this.value = value;
  }

  
  public OIMAttributeTO() {}

  
  public String getName() { return this.name; }


  
  public void setName(String name) { this.name = name; }


  
  public Object getValue() { return this.value; }


  
  public void setValue(Object value) { this.value = value; }



  
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("OIMAttributeTO [name=");
    builder.append(this.name);
    builder.append(", value=");
    builder.append(this.value);
    builder.append("]");
    return builder.toString();
  }
}
