/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.jsontos;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessOrder;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Owner
 */
@XmlRootElement(name = "rolemembership")
@XmlAccessorOrder(XmlAccessOrder.ALPHABETICAL)
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleMembershipTO {
    
    private String roleName;
    private String roleMember;
    private List<String> roleMembers;
    private List<String> memberRoles;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleMember() {
        return roleMember;
    }

    public void setRoleMember(String roleMember) {
        this.roleMember = roleMember;
    }

    public List<String> getRoleMembers() {
        return roleMembers;
    }

    public void setRoleMembers(List<String> roleMembers) {
        this.roleMembers = roleMembers;
    }

    public List<String> getMemberRoles() {
        return memberRoles;
    }

    public void setMemberRoles(List<String> memberRoles) {
        this.memberRoles = memberRoles;
    }
    
    
    
    
}
