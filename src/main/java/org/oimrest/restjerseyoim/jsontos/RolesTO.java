/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.jsontos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessOrder;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "role-list")
@XmlAccessorOrder(XmlAccessOrder.ALPHABETICAL)
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RolesTO {

    @XmlElementWrapper(name = "roles")
    @XmlElement(name = "roles")
    private List<RoleTO> roles;
    @JsonIgnore
    private int failedRequestCount;

    public RolesTO() {
    }

    public List<RoleTO> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleTO> roles) {
        this.roles = roles;
    }

    public int getFailedRequestCount() {
        return this.failedRequestCount;
    }

    public void setFailedRequestCount(int failedRequestCount) {
        this.failedRequestCount = failedRequestCount;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RolesTO [roles=");
        builder.append(this.roles);
        builder.append("]");
        return builder.toString();
    }
}
