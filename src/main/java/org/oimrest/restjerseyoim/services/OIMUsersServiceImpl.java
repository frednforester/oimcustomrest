/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.services;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;
import org.oimrest.restjerseyoim.jsontos.ErrorInfo;
import org.oimrest.restjerseyoim.jsontos.OIMAttributeTO;
import org.oimrest.restjerseyoim.jsontos.OperationStatus;
import org.oimrest.restjerseyoim.jsontos.ResponseException;
import org.oimrest.restjerseyoim.jsontos.UserTO;
import org.oimrest.restjerseyoim.jsontos.UsersTO;
import org.oimrest.restjerseyoim.oimservices.IUserOperations;
import org.oimrest.restjerseyoim.utils.ConverterUtil;

/**
 *
 * @author Owner
 */
@Path("/users")
public class OIMUsersServiceImpl {
    
    private static final Logger logger = Logger.getLogger(OIMUsersServiceImpl.class.getName());
    
    
    
    @Inject
    public IUserOperations userOps;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response searchUsers(@Context ContainerRequestContext context) throws ResponseException {
        logger.debug("searchUsers");
        
        
        LinkedList query = (LinkedList)context.getProperty("query");
        if (query == null)
        {
            OperationStatus status = new OperationStatus();
            status.setMessage("Invalid query string");
            status.setStatus(OperationStatus.STATUS_FAILURE);
            return Response.status(Response.Status.OK).entity(status).build();
        }
        
        logger.debug("query type:" + query.getClass().getName());
        logger.debug("query:" + query);
        String qString = "";
        Iterator iter = query.iterator();
        while (iter.hasNext()) {
            // appending using "+" operator
            qString = qString + iter.next() + " ";
        }
        
        try
        {
            //UserOperationsImpl userOps = new UserOperationsImpl();
            UsersTO users = userOps.searchUsers(qString);
            logger.debug("users:" + users);
            OperationStatus status = new OperationStatus();
            status.setMessage(OperationStatus.STATUS_SUCCESS);
            status.setStatus(OperationStatus.STATUS_SUCCESS);
            //return Response.status(Response.Status.OK).entity(users).build();
            return Response.ok().entity(users).build();
        }
        catch(Exception e)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("API ERROR");
            error.setMessage(e.getMessage());
            logger.error("API ERROR:" + e.getMessage(),e);
            throw new ResponseException("API ERROR:" + e.getMessage(),error);
        }
        
        /*
        Node rootNode = new RSQLParser().parse(query.toString());
        logger.debug("rootNode:" + rootNode.toString());
        Object spec = rootNode.accept(new CustomRsqlVisitor());
        logger.debug("spec:" + spec);
        OperationStatus status = new OperationStatus();
        status.setMessage(OperationStatus.STATUS_SUCCESS);
        status.setStatus(OperationStatus.STATUS_SUCCESS);
        return Response.status(Response.Status.OK).entity(status).build();
        */
    }
    
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(UserTO userTo, @Context ContainerRequestContext context) throws ResponseException {
        logger.debug("createUser");
        
        if (userTo.getFields()== null || userTo.getFields().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. user attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        List<OIMAttributeTO> attributes = userTo.getFields();
        
        Map oimAtributes = null;
                
        try
        {
            oimAtributes = ConverterUtil.getAttrMapFromList(attributes);
        }
        catch(Exception e)
        {
            logger.error("Invalid Data:");
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. user attributes must be specified");
            error.setMessage("Invalid Parameters:" + e.getMessage());
            throw new ResponseException("Invalid Parameters",error);
        }
        
        try
        {
            UserTO result = userOps.createUser(userTo);
            logger.debug("result:" + result);
            return Response.ok().entity(result).build();
        }
        catch(Exception e)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("API ERROR");
            error.setMessage(e.getMessage());
            logger.error("API ERROR:" + e.getMessage(),e);
            throw new ResponseException("API ERROR:" + e.getMessage(),error);
        }
      
    }

    @PUT
    @Path("/{uid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUser(@PathParam("uid") String uid,UserTO userTo, @Context ContainerRequestContext context) throws ResponseException {
        logger.debug("updateUser");
        
        if (uid == null)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. UserId or UserKey must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        if (userTo == null)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. user data must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        if (userTo.getFields()== null || userTo.getFields().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. user attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        List<OIMAttributeTO> attributes = userTo.getFields();
        
        Map oimAtributes = null;
                
        try
        {
            oimAtributes = ConverterUtil.getAttrMapFromList(attributes);
        }
        catch(Exception e)
        {
            logger.error("Invalid Data:");
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. user attributes must be specified");
            error.setMessage("Invalid Parameters:" + e.getMessage());
            throw new ResponseException("Invalid Parameters",error);
        }
        
        
        if (userTo.getUserLogin()== null)
        {
            userTo.setUserLogin(uid);
        }
        
        try
        {
            OperationStatus result = userOps.updateUser(userTo);
            logger.debug("result:" + result);
            return Response.ok().entity(result).build();
        }
        catch(Exception e)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("API ERROR");
            error.setMessage(e.getMessage());
            logger.error("API ERROR:" + e.getMessage(),e);
            throw new ResponseException("API ERROR:" + e.getMessage(),error);
        }
    }

    @GET
    @Path("/{uid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("uid") String uid, @Context ContainerRequestContext context) throws ResponseException {
        logger.debug("getUser");
        
        if (uid == null)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. UserId or UserKey must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        uid = Jsoup.clean(uid,Safelist.none());
        if (uid == null || uid.trim().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. UserId or UserKey contains invalid data");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        try
        {
            UserTO newUserTo = userOps.getUser(uid);
            logger.debug("userTo:" + newUserTo);
            return Response.ok().entity(newUserTo).build();
        }
        catch(Exception e)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("API ERROR");
            error.setMessage(e.getMessage());
            logger.error("API ERROR:" + e.getMessage(),e);
            throw new ResponseException("API ERROR:" + e.getMessage(),error);
        }
    }

    @DELETE
    @Path("/{uid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("uid") String uid, @Context ContainerRequestContext context) throws ResponseException {
        logger.debug("deleteUser");
        logger.debug("uid:" + uid);
        OperationStatus status = new OperationStatus();
        status.setMessage("No Supported");
        status.setStatus(OperationStatus.STATUS_FAILURE);
        return Response.status(Response.Status.BAD_REQUEST).entity(status).build();
    }
    
}
