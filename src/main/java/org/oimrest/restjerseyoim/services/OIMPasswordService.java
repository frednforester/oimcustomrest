/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.services;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.oimrest.restjerseyoim.jsontos.ChangeUserPwdTO;
import org.oimrest.restjerseyoim.jsontos.ErrorInfo;
import org.oimrest.restjerseyoim.jsontos.OperationStatus;
import org.oimrest.restjerseyoim.jsontos.ResponseException;
import org.oimrest.restjerseyoim.oimservices.IPasswordOperations;

/**
 *
 * @author Owner
 */
@Path("/password")
public class OIMPasswordService {
    
    private static final Logger logger = Logger.getLogger(OIMPasswordService.class.getName());
    
    
    @Inject
    public IPasswordOperations passwordOps;
    
    @PUT
    @Path("/{uid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response changeUserPassword(@PathParam("uid") String userLogin,ChangeUserPwdTO passwordInfo,@Context ContainerRequestContext context) throws ResponseException
    {
        logger.debug("searchUsers");
       
        if (passwordInfo == null)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. password data must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        if (passwordInfo.getNewPassword() == null || passwordInfo.getNewPassword().trim().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. user attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        if (passwordInfo.getOldPassword() == null || passwordInfo.getOldPassword().trim().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. user attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        if (passwordInfo.getConfirmPassword()== null || passwordInfo.getConfirmPassword().trim().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. user attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        try
        {
            passwordOps.changeUserPassword(userLogin, passwordInfo.getNewPassword());
            OperationStatus status = new OperationStatus();
            String msg = "Password Updated";
            status.setMessage(msg);
            status.setStatus(OperationStatus.STATUS_SUCCESS);
            logger.debug("Password Updated");
            return Response.ok().entity(status).build();
        }
        catch(Exception e)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("API ERROR");
            error.setMessage(e.getMessage());
            logger.error("API ERROR:" + e.getMessage(),e);
            throw new ResponseException("API ERROR:" + e.getMessage(),error);
        }
        
    }
    
    
    
}
