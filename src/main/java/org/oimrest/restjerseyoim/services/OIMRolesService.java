/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.services;

import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import oracle.iam.identity.exception.NoSuchRoleException;
import oracle.iam.identity.exception.RoleAlreadyExistsException;
import oracle.iam.identity.exception.RoleCreateException;
import oracle.iam.identity.exception.RoleDeleteException;
import oracle.iam.identity.exception.RoleSearchException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import org.apache.log4j.Logger;
import org.oimrest.restjerseyoim.jsontos.ErrorInfo;
import org.oimrest.restjerseyoim.jsontos.OIMAttributeTO;
import org.oimrest.restjerseyoim.jsontos.OperationStatus;
import org.oimrest.restjerseyoim.jsontos.ResponseException;
import org.oimrest.restjerseyoim.jsontos.RoleTO;
import org.oimrest.restjerseyoim.jsontos.RolesTO;
import org.oimrest.restjerseyoim.oimservices.IRoleOperations;
import org.oimrest.restjerseyoim.utils.ConverterUtil;


@Path("/roles")
public class OIMRolesService {
    
    private static final Logger logger = Logger.getLogger(OIMRolesService.class.getName());
    
    @Inject
    public IRoleOperations roleOps;
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createRole(RoleTO roleTo, @Context ContainerRequestContext context) throws ResponseException {
        
        if (roleTo == null)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role data must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        if (roleTo.getName() == null || roleTo.getName().trim().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role name must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        if (roleTo.getCategoryName() == null || roleTo.getCategoryName().trim().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role category must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        if (roleTo.getDisplayName() == null || roleTo.getDisplayName().trim().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role display name must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        try 
        {
            OperationStatus status = roleOps.createRole(roleTo);
            return Response.ok().entity(status).build();
            
        } 
        catch (ValidationFailedException e) {
            logger.error("ValidationFailedException:" + e.getMessage(),e);
            ErrorInfo error = new ErrorInfo();
            error.setCode("ValidationFailedException");
            error.setMessage(e.getMessage());
            throw new ResponseException("ValidationFailedException:" + e.getMessage(),error);
        } catch (AccessDeniedException e) {
            logger.error("AccessDeniedException:" + e.getMessage(),e);
            ErrorInfo error = new ErrorInfo();
            error.setCode("AccessDeniedException");
            error.setMessage(e.getMessage());
            throw new ResponseException("AccessDeniedException:" + e.getMessage(),error);
        } catch (RoleAlreadyExistsException e) {
            logger.error("RoleAlreadyExistsException:" + e.getMessage(),e);
            ErrorInfo error = new ErrorInfo();
            error.setCode("RoleAlreadyExistsException");
            error.setMessage(e.getMessage());
            throw new ResponseException("RoleAlreadyExistsException:" + e.getMessage(),error);
        } catch (RoleCreateException e) {
            logger.error("RoleCreateException:" + e.getMessage(),e);
            ErrorInfo error = new ErrorInfo();
            error.setCode("RoleCreateException");
            error.setMessage(e.getMessage());
            throw new ResponseException("RoleCreateException:" + e.getMessage(),error);
        }
        
        
    }
    
    @GET
    @Path("/query/{roleName}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAllRoles(@PathParam("roleName") String roleName,@Context ContainerRequestContext context) throws ResponseException {
        logger.debug("getAllUsers");
        try
        {
            RolesTO rolesTo = roleOps.getAllRoles(roleName);
            return Response.ok().entity(rolesTo).build();
        }
        catch(RoleSearchException e)
        {
            logger.error("RoleSearchException:" + e.getMessage(),e);
            ErrorInfo error = new ErrorInfo();
            error.setCode("RoleSearchException");
            error.setMessage(e.getMessage());
            throw new ResponseException("RoleSearchException:" + e.getMessage(),error);
        }
        catch(AccessDeniedException e)
        {
            logger.error("AccessDeniedException:" + e.getMessage(),e);
            ErrorInfo error = new ErrorInfo();
            error.setCode("AccessDeniedException");
            error.setMessage(e.getMessage());
            throw new ResponseException("AccessDeniedException:" + e.getMessage(),error);
        }
        
    }
    
    @PUT
    @Path("/{roleName}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateRole(@PathParam("roleName") String roleName, RoleTO roleTo,@Context ContainerRequestContext context) throws ResponseException {
        logger.debug("updateRole");
        logger.debug("roleName:" + roleName);
        
        if (roleName == null)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. UserId or UserKey must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        if (roleTo == null)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role data must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        if (roleTo.getFields()== null || roleTo.getFields().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        List<OIMAttributeTO> attributes = roleTo.getFields();
        
        Map oimAtributes = null;
                
        try
        {
            oimAtributes = ConverterUtil.getAttrMapFromList(attributes);
        }
        catch(Exception e)
        {
            logger.error("Invalid Data:");
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role attributes must be specified");
            error.setMessage("Invalid Parameters:" + e.getMessage());
            throw new ResponseException("Invalid Parameters",error);
        }
        roleTo.setName(roleName);
        
        try
        {
            OperationStatus result = roleOps.updateByName(roleTo);
            logger.debug("result:" + result);
            return Response.ok().entity(result).build();
        }
        catch(Exception e)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("API ERROR");
            error.setMessage(e.getMessage());
            logger.error("API ERROR:" + e.getMessage(),e);
            throw new ResponseException("API ERROR:" + e.getMessage(),error);
        }
        
    }
    
    @GET
    @Path("/{roleName}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getRole(@PathParam("roleName") String roleName, @Context ContainerRequestContext context) throws ResponseException {
        logger.debug("getUser");
        logger.debug("getAllUsers");
        try
        {
            RoleTO roleTo = roleOps.getRole(roleName);
            return Response.ok().entity(roleTo).build();
        }
        catch(RoleSearchException e)
        {
            logger.error("RoleSearchException:" + e.getMessage(),e);
            ErrorInfo error = new ErrorInfo();
            error.setCode("RoleSearchException");
            error.setMessage(e.getMessage());
            throw new ResponseException("RoleSearchException:" + e.getMessage(),error);
        }
        catch(AccessDeniedException e)
        {
            logger.error("AccessDeniedException:" + e.getMessage(),e);
            ErrorInfo error = new ErrorInfo();
            error.setCode("AccessDeniedException");
            error.setMessage(e.getMessage());
            throw new ResponseException("AccessDeniedException:" + e.getMessage(),error);
        }
        catch(Exception e)
        {
            logger.error("Exception:" + e.getMessage(),e);
            ErrorInfo error = new ErrorInfo();
            error.setCode("Exception");
            error.setMessage(e.getMessage());
            throw new ResponseException("Exception:" + e.getMessage(),error);
        }
    }
    
    @DELETE
    @Path("/{roleName}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteRole(@PathParam("roleName") String roleName, @Context ContainerRequestContext context) throws ResponseException {
        logger.debug("deleteUser");
        
        try {
            OperationStatus status = roleOps.deleteRole(roleName);
            return Response.ok().entity(status).build();
        } catch (ValidationFailedException ex) {
            logger.error("ValidationFailedException:" + ex.getMessage(),ex);
            ErrorInfo error = new ErrorInfo();
            error.setCode("ValidationFailedException");
            error.setMessage(ex.getMessage());
            throw new ResponseException("ValidationFailedException:" + ex.getMessage(),error);
        } catch (AccessDeniedException ex) {
            logger.error("AccessDeniedException:" + ex.getMessage(),ex);
            ErrorInfo error = new ErrorInfo();
            error.setCode("AccessDeniedException");
            error.setMessage(ex.getMessage());
            throw new ResponseException("AccessDeniedException:" + ex.getMessage(),error);
        } catch (RoleDeleteException ex) {
            logger.error("RoleDeleteException:" + ex.getMessage(),ex);
            ErrorInfo error = new ErrorInfo();
            error.setCode("RoleDeleteException");
            error.setMessage(ex.getMessage());
            throw new ResponseException("RoleDeleteException:" + ex.getMessage(),error);
        } catch (NoSuchRoleException ex) {
            logger.error("NoSuchRoleException:" + ex.getMessage(),ex);
            ErrorInfo error = new ErrorInfo();
            error.setCode("NoSuchRoleException");
            error.setMessage(ex.getMessage());
            throw new ResponseException("NoSuchRoleException:" + ex.getMessage(),error);
        }
    }
    
}
