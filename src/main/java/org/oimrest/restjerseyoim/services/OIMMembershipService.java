/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.services;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.oimrest.restjerseyoim.jsontos.ErrorInfo;
import org.oimrest.restjerseyoim.jsontos.OperationStatus;
import org.oimrest.restjerseyoim.jsontos.ResponseException;
import org.oimrest.restjerseyoim.jsontos.RoleMembershipTO;
import org.oimrest.restjerseyoim.oimservices.IMembershipOperations;

/**
 *
 * @author Owner
 */
@Path("/members")
public class OIMMembershipService {
    
    
    private static final Logger logger = Logger.getLogger(OIMMembershipService.class.getName());
    
    
    @Inject
    public IMembershipOperations memberOps;
    
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addRoleMember(RoleMembershipTO membershipTo,@Context ContainerRequestContext context) throws ResponseException {
        
        if (membershipTo == null)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. RoleMembershipTO attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        if (membershipTo.getRoleMember() == null || membershipTo.getRoleMember().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role member attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        if (membershipTo.getRoleName()== null || membershipTo.getRoleName().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role name attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        try {
            OperationStatus result = memberOps.addRoleMember(membershipTo);
            return Response.ok().entity(result).build();
        } 
        catch (Exception e) {
            ErrorInfo error = new ErrorInfo();
            error.setCode("API ERROR");
            error.setMessage(e.getMessage());
            logger.error("API ERROR:" + e.getMessage(),e);
            throw new ResponseException("API ERROR:" + e.getMessage(),error);
        } 
        
    }
    
    @GET
    @Path("/queryusers/{roleName}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getRoleMembers(@PathParam("roleName") String roleName,@Context ContainerRequestContext context) throws ResponseException {
        
        if (roleName== null || roleName.isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role name attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        try {
            RoleMembershipTO memberships = memberOps.getRoleMembers(roleName);
            return Response.ok().entity(memberships).build();
        } 
        catch (Exception e) {
            ErrorInfo error = new ErrorInfo();
            error.setCode("API ERROR");
            error.setMessage(e.getMessage());
            logger.error("API ERROR:" + e.getMessage(),e);
            throw new ResponseException("API ERROR:" + e.getMessage(),error);
        }
    }
    
    @GET
    @Path("/queryroles/{userLogin}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getMembersRoles(@PathParam("userLogin") String userLogin,@Context ContainerRequestContext context) throws ResponseException {
        
        if (userLogin == null || userLogin.isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role name attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        try {
            RoleMembershipTO memberships = memberOps.getMembersRoles(userLogin);
            return Response.ok().entity(memberships).build();
        } 
        catch (Exception e) {
            ErrorInfo error = new ErrorInfo();
            error.setCode("API ERROR");
            error.setMessage(e.getMessage());
            logger.error("API ERROR:" + e.getMessage(),e);
            throw new ResponseException("API ERROR:" + e.getMessage(),error);
        }
    }
    
    @DELETE
    @Path("/{roleName}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteRoleMember(RoleMembershipTO membershipTo, @Context ContainerRequestContext context) throws ResponseException {
        
        if (membershipTo == null)
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. RoleMembershipTO attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        if (membershipTo.getRoleMember() == null || membershipTo.getRoleMember().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role member attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        if (membershipTo.getRoleName()== null || membershipTo.getRoleName().isEmpty())
        {
            ErrorInfo error = new ErrorInfo();
            error.setCode("Invalid Parameters. role name attributes must be specified");
            error.setMessage("Invalid Parameters");
            throw new ResponseException("Invalid Parameters",error);
        }
        
        try {
            OperationStatus result = memberOps.deleteRoleMember(membershipTo);
            return Response.ok().entity(result).build();
        } 
        catch (Exception e) {
            ErrorInfo error = new ErrorInfo();
            error.setCode("API ERROR");
            error.setMessage(e.getMessage());
            logger.error("API ERROR:" + e.getMessage(),e);
            throw new ResponseException("API ERROR:" + e.getMessage(),error);
        } 
    
    }
    
}
