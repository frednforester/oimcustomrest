/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.services;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.oimrest.restjerseyoim.oimservices.ITesterOperations;

/**
 *
 * @author Owner
 */
@Path("/tester")
public class TesterServiceImpl {
    
    private static final Logger logger = Logger.getLogger(TesterServiceImpl.class.getName());
    
    
    @Inject
    private ITesterOperations testerOps;
    
    /*
    @Inject
    @Named("oimClient")
    public TesterServiceImpl(OIMClientFactory oimClientFactory) {
        this.oimClientFactory = oimClientFactory;
    }
*/
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/testMe/{value}")
    public Response testMe(@PathParam("value") String value,@Context ContainerRequestContext context, @Context HttpServletRequest request) {
        logger.debug("testMe:");
        try
        {
            this.testerOps.testMe();
        }
        catch(Exception e)
        {
            return Response.serverError().build();
        }
        
        /*
        try
        {
            UserPrincipal user = (UserPrincipal)context.getProperty("UserPrincipal");
            logger.debug("USER:" + user);
            OIMClient client = oimClientFactory.getOIMClient(user);
        }
        catch(Exception e)
        {
            return Response.serverError().build();
        }*/
        return Response.ok().build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response basicTest(@Context ContainerRequestContext context,@Context HttpServletRequest request) {

        logger.debug("basicTest:");
        return Response.ok().build();
        
    }
    
    
}
