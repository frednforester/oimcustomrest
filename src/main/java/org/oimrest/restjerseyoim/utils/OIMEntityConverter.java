/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import oracle.iam.identity.rolemgmt.api.RoleManagerConstants;
import oracle.iam.identity.rolemgmt.vo.Role;
import oracle.iam.identity.usermgmt.vo.User;
import org.oimrest.restjerseyoim.jsontos.OIMAttributeTO;
import org.oimrest.restjerseyoim.jsontos.RoleTO;
import org.oimrest.restjerseyoim.jsontos.UserTO;

/**
 *
 * @author Owner
 */
public class OIMEntityConverter {

    public static UserTO getUserTO(User userVO) {
        String methodName = "getUserTO";

        List<OIMAttributeTO> attributes = ConverterUtil.getNotNullAttributes(userVO.getAttributes());

        UserTO userTO = new UserTO();

        Map<String, Object> allAttributesMap = ConverterUtil.getAttrObjectMapFromList(attributes);

        //formatDate(allAttributesMap, context);
        HashMap<String, Object> selectedAttributesMap = new HashMap<String, Object>();

        for (Map.Entry<String, Object> entry : allAttributesMap.entrySet()) {
            if (entry.getValue() != null) {
                selectedAttributesMap.put(entry.getKey(), entry.getValue());
            }
        }

        userTO.setFields(ConverterUtil.getNotNullAttributesAllTypes(allAttributesMap));

        return userTO;
    }
    
    
    public static RoleTO getRoleTO(Role roleVO) {
        
        RoleTO toRole = new RoleTO();
        HashMap<String, Object> attrMap = roleVO.getAttributes();
        toRole.setFields(ConverterUtil.getNotNullAttributes(attrMap));
        String id = roleVO.getEntityId();
        toRole.setId(id);
        toRole.setName(roleVO.getName());
        toRole.setName((String)roleVO.getAttribute(RoleManagerConstants.ROLE_NAME));

        HashMap<String, Object> map = roleVO.getAttributes();
        
        if (map.containsKey("Role Key")) {
            map.put("Role Key", null);
        }
        
        if (map.containsKey("ROLE_NAME") || map.containsKey("Role Name"))
        {
            toRole.setName((String)map.get("ROLE_NAME"));
        }
        Long owner = (Long) roleVO.getAttributes().get("Role Owner Key");

        if (owner != null) {

            OIMAttributeTO ownerLogin = new OIMAttributeTO("Role Owner Key", owner);
            toRole.getFields().add(ownerLogin);
        }

        return toRole;
    }


}
