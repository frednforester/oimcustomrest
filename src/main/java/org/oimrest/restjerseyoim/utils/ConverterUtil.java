/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;
import org.oimrest.restjerseyoim.jsontos.OIMAttributeTO;

/**
 *
 * @author Owner
 */
public class ConverterUtil {
    
    private static final String DATE_TYPE = "Date";
  
  public static HashMap<String, Object> getNotNullAttributeMap(Map<String, Object> attributes) {
    HashMap<String, Object> map = new HashMap<String, Object>();
    for (Map.Entry<String, Object> entry : attributes.entrySet()) {
      if (entry.getValue() != null) {
        map.put(entry.getKey(), entry.getValue());
      }
    } 
    return map;
  }

  
  public static List<OIMAttributeTO> getNotNullAttributes(Map<String, Object> attributes) {
    List<OIMAttributeTO> oimAttributes = new ArrayList<OIMAttributeTO>();
    
    for (Map.Entry<String, Object> entry : attributes.entrySet()) {
      Object attributeValue = entry.getValue();
      if (null != attributeValue) {
        OIMAttributeTO attribute = new OIMAttributeTO();
        attribute.setName((String)entry.getKey());
        attribute.setValue(attributeValue);
        
        oimAttributes.add(attribute);
      } 
    } 
    return oimAttributes;
  }
  
   public static List<OIMAttributeTO> getFieldsToBeReturned(Map<String, Object> attributes, boolean isAllowNull, Set<String> excludeAttributes) {
    List<OIMAttributeTO> oimAttributes = new ArrayList<OIMAttributeTO>();
    if (excludeAttributes != null && excludeAttributes.size() > 0) {
      attributes.keySet().removeAll(excludeAttributes);
    }
    for (Map.Entry<String, Object> entry : attributes.entrySet()) {
      if (isAllowNull || null != entry.getValue()) {
        OIMAttributeTO attribute = new OIMAttributeTO();
        attribute.setName((String)entry.getKey());
        attribute.setValue(entry.getValue());
        oimAttributes.add(attribute);
      } 
    } 
    
    return oimAttributes;
  }


  
  public static Map<String, String> getAttrMapFromList(List<OIMAttributeTO> attrList)  {
    Map<String, String> attrMap = new HashMap<String, String>();
    if (attrList == null) {
      return attrMap;
    }
    
    for (OIMAttributeTO oimAttributeTO : attrList) {
      if (oimAttributeTO.getName() != null && !oimAttributeTO.getName().isEmpty()) {
        
        String value = (oimAttributeTO.getValue() != null) ? oimAttributeTO.getValue().toString() : null;
        String name = oimAttributeTO.getName();
        if (value != null)
        {
            name = Jsoup.clean(name, Safelist.none());
            value = Jsoup.clean(value, Safelist.none());
        }
        attrMap.put(name, value);
        continue;
      } 
      //throw new Exception("InvalidInputException");
    } 

    
    return attrMap;
  }


  
  public static Map<String, String> getStringAttrMapFromList(List<OIMAttributeTO> attrList) throws Exception {
    Map<String, String> attrMap = new HashMap<String, String>();
    if (attrList == null) {
      return attrMap;
    }
    
    for (OIMAttributeTO oimAttributeTO : attrList) {
      if (oimAttributeTO.getName() != null && !oimAttributeTO.getName().isEmpty()) {
        
        String value = (oimAttributeTO.getValue() != null) ? oimAttributeTO.getValue().toString() : null;
        attrMap.put(oimAttributeTO.getName(), value);
        continue;
      } 
      throw new Exception("InvalidInputException");
    } 

    
    return attrMap;
  }

  
  public static Map<String, Object> getAttrObjectMapFromList(List<OIMAttributeTO> attrList) {
    Map<String, Object> attrMap = new HashMap<String, Object>();
    
    for (OIMAttributeTO oimAttributeTO : attrList) {
      attrMap.put(oimAttributeTO.getName(), oimAttributeTO.getValue());
    }
    return attrMap;
  }
  
  public static List<OIMAttributeTO> getNotNullAttributesAllTypes(Map<String, ?> attributes) {
    List<OIMAttributeTO> oimAttributes = new ArrayList<OIMAttributeTO>();
    
    for (Map.Entry<String, ?> entry : attributes.entrySet()) {
      if (null != entry.getValue()) {
        OIMAttributeTO attribute = new OIMAttributeTO();
        attribute.setName((String)entry.getKey());
        attribute.setValue(entry.getValue());
        oimAttributes.add(attribute);
      } 
    } 
    return oimAttributes;
  }

  
  public static List<List<OIMAttributeTO>> getNotNullAttributes(List<Map<String, Object>> attributesList) {
    List<List<OIMAttributeTO>> returnList = new ArrayList<List<OIMAttributeTO>>();
    if (attributesList == null) {
      return returnList;
    }
    for (Map<String, Object> attributes : attributesList) {
      List<OIMAttributeTO> oimAttributes = new ArrayList<OIMAttributeTO>();
      
      for (Map.Entry<String, Object> entry : attributes.entrySet()) {
        if (null != entry.getValue()) {
          OIMAttributeTO attribute = new OIMAttributeTO();
          attribute.setName((String)entry.getKey());
          attribute.setValue(entry.getValue());
          oimAttributes.add(attribute);
        } 
      } 
      returnList.add(oimAttributes);
    } 
    
    return returnList;
  }
  
  
}
