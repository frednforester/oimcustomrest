/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.oim;

import oracle.iam.platform.OIMClient;
import org.glassfish.jersey.spi.Contract;
import org.oimrest.restjerseyoim.model.UserPrincipal;

/**
 *
 * @author Owner
 */
@Contract
public interface IOIMClientFactory {

    /**
     *
     * @param userProps
     * @return
     */
    public OIMClient getOIMClient(UserPrincipal userProps) throws Exception;
    /**
     * you should call logout when done to free up the internal DB connection
     *
     * @param client
     */
    void logout(OIMClient client);

    /**
     *
     * @param OIMInitialContextFactory
     */
    void setOIMInitialContextFactory(String OIMInitialContextFactory);

    /**
     *
     * @param OIMPassword
     */
    void setOIMPassword(String OIMPassword);

    /**
     *
     * @param OIMURL
     */
    void setOIMURL(String OIMURL);

    /**
     *
     * @param OIMUserName
     */
    void setOIMUserName(String OIMUserName);

    /**
     *
     * @param xlAuthLogin
     */
    void setXlAuthLogin(String xlAuthLogin);
    
}
