/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.oim;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import oracle.iam.platform.OIMClient;
import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;
import org.oimrest.restjerseyoim.context.MyApplicationSecurityContext;
import org.oimrest.restjerseyoim.model.UserPrincipal;

/**
 *
 * @author Owner
 */
@Service
public class OIMClientWrapper implements IOIMClientWrapper {
    
    private static final Logger logger = Logger.getLogger(OIMClientWrapper.class.getName());
    
    /*
    @Inject
    private IOIMClientFactory oimClientFactory;
    
    @Context
    private SecurityContext secContext;
    */
    @Context
    private ContainerRequestContext context2;
    
    private OIMClient client;

    @Override
    public OIMClient getClient() {
        MyApplicationSecurityContext mine = (MyApplicationSecurityContext)context2.getSecurityContext();
        logger.debug("mysec:" + mine.getClass().getName());
        this.client = mine.getClient();
        return client;
    }
    
}
