/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.oim;

import oracle.iam.platform.OIMClient;
import org.glassfish.jersey.spi.Contract;

/**
 *
 * @author Owner
 */
@Contract
public interface IOIMClientWrapper {
    
    public OIMClient getClient();
    
}
