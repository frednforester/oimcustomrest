/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.oimrest.restjerseyoim.oim;

import Thor.API.Security.XLClientSecurityAssociation;
import java.util.Hashtable;
import java.util.Properties;
import javax.inject.Named;
import javax.security.auth.login.LoginException;
import oracle.iam.platform.OIMClient;
import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;
import org.oimrest.restjerseyoim.model.UserPrincipal;

/**
 * Used as a helper for making remote OIM Connections
 */
@Service @Named("oimClient")
public class OIMClientFactory implements IOIMClientFactory {

    private static final Logger logger = Logger.getLogger(OIMClientFactory.class.getName());
    private String OIMUserName;
    private String OIMPassword;
    private String OIMURL;
    private String OIMInitialContextFactory;
    private final String defaultConfigFile="jndi_en_US.properties";
    private final String defaultAuthWLFile="authwl_en_US.conf";
    
    // sys props
    private String xlHomeDir;
    private String xlAuthLogin;

    
    private boolean connected;

    public OIMClient getOIMClient(UserPrincipal userProps) throws Exception
    {
        logger.debug("userProps:" + userProps);
        try
        {
            loadConfig(null);
            setOIMUserName(userProps.getLogin());
            setOIMPassword(userProps.getPassword());
            return loginWithCustomEnv();
        }
        catch(Exception e)
        {
            logger.error("API Error:" + e.getMessage(),e);
            throw e;
        }
    }

    /**
     * authenticate based on the values set via the setters
     * OIMUserName
     * OIMPassword
     * OIMURL
     * OIMInitialContextFactory
     * @throws OIMHelperException 
     */
    private OIMClient loginWithCustomEnv() throws Exception {

        logger.debug("Creating client....");

        if (!validate())
            throw new Exception("Invalid Connection Args");

        Hashtable env = new Hashtable();

        
        env.put(OIMClient.JAVA_NAMING_FACTORY_INITIAL,OIMInitialContextFactory);
        env.put(OIMClient.JAVA_NAMING_PROVIDER_URL, OIMURL);
        System.setProperty("java.security.auth.login.config",xlAuthLogin);
        System.setProperty("weblogic.MaxMessageSize", "2000000000");
        String type = System.getenv("APPSERVER_TYPE");
        logger.debug("APPSERVER_TYPE:" + type);
        if (type == null)
            logger.info("Expect the JRF error unless you add APPSERVER_TYPE=wls to the OS env");
        System.setProperty("APPSERVER_TYPE", "wls");
        OIMClient client = new OIMClient(env);
        logger.debug("Logging in");
        try {
            client.login(OIMUserName, OIMPassword.toCharArray());
            XLClientSecurityAssociation.setClientHandle(client);
        } catch (LoginException ex) {
            logger.error("LoginException",ex);
            throw new Exception("LoginException",ex);
        }
        connected=true;
        logger.debug("Log in successful");
        return client;

    }

    /**
     * use a config file other than jndi.properties
     * @param fileName
     * @throws OIMHelperException 
     */
    private void loadConfig(String fileName) throws Exception
    {
        if (fileName == null || fileName.trim().length() == 0)
            fileName = defaultConfigFile;

        WebConfigLoader configLoader = new WebConfigLoader();
        try
        {
            configLoader.getConfig(fileName);
        }
        catch(Exception ex)
        {
            logger.error("OIMHelperException",ex);
            throw ex;
        }

        Properties props = configLoader.getConfigProps();
        logger.debug("Props:" + props);
        setOIMInitialContextFactory(props.getProperty("java.naming.factory.initial"));
        setOIMPassword(props.getProperty("java.naming.security.credentials"));
        setOIMURL(props.getProperty("java.naming.provider.url"));
        setOIMUserName(props.getProperty("java.naming.security.principal"));
        setXlAuthLogin(props.getProperty("java.security.auth.login.config"));
        
        String authWLPath = configLoader.getAuthWLPath(this.defaultAuthWLFile);
        logger.debug("authWLPath:" + authWLPath);
        if (authWLPath != null)
        {
            setXlAuthLogin(authWLPath);
        }

    }
    
    /**
     * you should call logout when done to free up the internal DB connection
     * 
     */
    @Override
    public void logout(OIMClient client)
    {
        logger.debug("Closing Connection");
        
        if (client != null && connected)
        {
            client.logout();
            connected=false;
        }
    }

    /**
     * 
     * @param OIMInitialContextFactory 
     */
    @Override
    public void setOIMInitialContextFactory(String OIMInitialContextFactory) {
        this.OIMInitialContextFactory = OIMInitialContextFactory;
    }

    /**
     * 
     * @param OIMPassword 
     */
    @Override
    public void setOIMPassword(String OIMPassword) {
        this.OIMPassword = OIMPassword;
    }

    /**
     * 
     * @param OIMURL 
     */
    @Override
    public void setOIMURL(String OIMURL) {
        this.OIMURL = OIMURL;
    }

    /**
     * 
     * @param OIMUserName 
     */
    @Override
    public void setOIMUserName(String OIMUserName) {
        this.OIMUserName = OIMUserName;
    }

    
    /**
     * 
     * @param xlAuthLogin 
     */
    @Override
    public void setXlAuthLogin(String xlAuthLogin) {
        this.xlAuthLogin = xlAuthLogin;
    }

    private boolean validate()
    {
        if (OIMPassword == null || OIMPassword.trim().length() == 0)
        {
            logger.debug("Missing OIMPassword");
            return false;
        }
        if (OIMUserName == null || OIMUserName.trim().length() == 0)
        {
            logger.debug("Missing OIMUserName");
            return false;
        }
        if (OIMURL == null || OIMURL.trim().length() == 0)
        {
            logger.debug("Missing OIMURL");
            return false;
        }
        if (OIMInitialContextFactory == null || OIMInitialContextFactory.trim().length() == 0)
        {
            logger.debug("Missing OIMInitialContextFactory");
            return false;
        }
        
        if (xlAuthLogin == null || xlAuthLogin.trim().length() == 0)
        {
            logger.debug("Missing xlAuthLogin");
            return false;
        }
        return true;
    }

}
