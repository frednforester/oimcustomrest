/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.oimrest.restjerseyoim.oim;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 */
public class WebConfigLoader {

    private String propertiesFile;
    private Properties configProps;

    private static final Logger logger = Logger.getLogger(WebConfigLoader.class.getName());
    
    public Properties getConfigProps() {
        return configProps;
    }
    
    public boolean getConfig(String fileName) throws Exception {

        String propertiesPath = "";
        
        if (fileName == null || fileName.trim().isEmpty())
        {
            throw new Exception("Invalid Properties File Name");
        }

        propertiesFile = fileName;

        // look on classpath
        logger.debug("search Classpath");
        InputStream configStream = this.getClass().getClassLoader().getResourceAsStream(propertiesFile);
        if (configStream != null) {
            logger.debug("got file from classpath");
            loadConfiguration(configStream);
                
            if (configProps != null)
            {
                return true;
            }
            
        }
        logger.error("File not in classpath");
        throw new Exception("File Not Found:" + propertiesFile);
        
    }
    
    public String getAuthWLPath(String fileName) throws Exception
    {
        URL url = getClass().getClassLoader().getResource(fileName);
        if (url == null)
        {
            logger.error(fileName + " Not Found!!");
            throw new Exception(fileName + " Not Found!");
            
        }
        return url.getPath();
    }

    private static byte[] getBytesFromFile(String FilePath)
            throws IOException {
        File file = new File(FilePath);
        InputStream is = new FileInputStream(file);

        long length = file.length();

        if (length > 2147483647L);
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;

        while ((offset < bytes.length) && ((numRead = is.read(bytes, offset, bytes.length - offset)) >= 0)) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }

    private boolean loadConfiguration(InputStream configStream) {
        configProps = new Properties();

        try {
            configProps.load(configStream);
        } catch (IOException e) {
            configProps = null;
            logger.error("Error Loading Config");
            return false;
        }

        return true;
    }



}
