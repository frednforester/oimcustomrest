/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status;
import oracle.iam.platform.OIMClient;
import org.apache.log4j.Logger;
import org.oimrest.restjerseyoim.context.MyApplicationSecurityContext;
import org.oimrest.restjerseyoim.model.UserPrincipal;
import org.oimrest.restjerseyoim.oim.OIMClientFactory;

/**
 *
 * @author Owner
 */
public class AuthFilter implements ContainerRequestFilter {
    
    private static final Logger logger = Logger.getLogger(AuthFilter.class.getName());
    
    @Override
    public void filter(ContainerRequestContext containerRequest) throws IOException {
        
        logger.debug("*********************** AuthFilter ****************");
        //GET, POST, PUT, DELETE, ...
        String method = containerRequest.getMethod();
        // myresource/get/56bCA for example
        String path = containerRequest.getUriInfo().getPath(true);
  
        //We do allow wadl to be retrieve
        if(method.equals("GET") && (path.equals("application.wadl") || path.equals("application.wadl/xsd0.xsd")))
        {
            return;
        }
        
        MultivaluedMap<String, String> queryParameters = containerRequest.getUriInfo().getQueryParameters();
        
        logger.debug("Query param – "+ queryParameters);
        Set<String> keys = queryParameters.keySet();
        
        for(String key : keys)
        {
            if ("query".equals(key))
            {
                containerRequest.setProperty("query", queryParameters.get(key));
                logger.debug("Setting filter value:" + queryParameters.get(key) );
            }
        }
        
        //Get the authentification passed in HTTP headers parameters
        String auth = containerRequest.getHeaderString("authorization");
  
        //If the user does not have the right (does not provide any HTTP Basic Auth)
        if(auth == null) {
            throw new WebApplicationException(Status.UNAUTHORIZED);
        }
  
        //lap : loginAndPassword
        String[] lap = BasicAuth.decode(auth);
        
        //If login or password fail
        if(lap == null || lap.length != 2) {
            throw new WebApplicationException(Status.UNAUTHORIZED);
        }
  
        //DO YOUR DATABASE CHECK HERE (replace that line behind)...
        UserPrincipal authUser =  new UserPrincipal();
        //authUser.setId("1");
        //authUser.setLogin("xelsysadm");
        authUser.setUrl("http://oim.testserver.org:14000");
        logger.debug("*********************** AuthFilter ****************");
        logger.debug("PW:" + lap[1]);
        logger.debug("U:" + lap[0]);
        logger.debug("*********************** AuthFilter ****************");
        authUser.setPassword(lap[1]);
        authUser.setLogin(lap[0]);
        String role = "ADMIN";
        List<String> roles = new ArrayList();
        roles.add(role);
        authUser.setRole(roles);
  
        //Our system refuse login and password
        if(authUser == null) {
            logger.error("Authentication Headers are Missing!!!!");
            throw new WebApplicationException(Status.UNAUTHORIZED);
        }
  
        // We configure your Security Context here
        String scheme = containerRequest.getUriInfo().getRequestUri().getScheme();
        MyApplicationSecurityContext myContext = new MyApplicationSecurityContext(authUser, scheme);
        
        OIMClientFactory factory = new OIMClientFactory();
        try
        {
            OIMClient client = factory.getOIMClient(authUser);
            myContext.setClient(client);
            containerRequest.setSecurityContext(myContext);
        }
        catch(Exception e)
        {
            throw new WebApplicationException(Status.UNAUTHORIZED);
        }
        containerRequest.setProperty("UserPrincipal", authUser);
        //TODO : HERE YOU SHOULD ADD PARAMETER TO REQUEST, TO REMEMBER USER ON YOUR REST SERVICE...
    }
}

    
    

