/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.restjerseyoim.model;

import java.security.Principal;
import java.util.List;

/**
 *
 * @author Owner
 */
public class UserPrincipal implements Principal {

    private String id;
    private String firstName;
    private String lastName;
    private String login;
    private String email;
    private String password;
    private String url;
    private List<String> role;
    
    @Override
    public String getName() {
        return this.id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getRole() {
        return role;
    }

    public void setRole(List<String> role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserPrincipal{" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", login=" + login + ", email=" + email + ", password=" + password + ", url=" + url + ", role=" + role + '}';
    }
    
  
}
