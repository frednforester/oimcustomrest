/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.oimrest.testers;

import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.Node;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.oimrest.restjerseyoim.context.rsql.CustomRsqlVisitor;

/**
 *
 * @author Owner
 */
public class TestParser {
    
    private static final Logger logger = Logger.getLogger(TestParser.class.getName());
    
    @Test
    public void mainTest()
    {
        //String query  = "User::Login==fforester and Display::Name==joe";
        String query  = "(User::Login==fforester or Display::Name==joe) and First::Name==Fred ";
        //String query  = "User::Login==fforester";
        Node rootNode = new RSQLParser().parse(query);
        logger.debug("rootNode:" + rootNode.toString());
        SearchCriteria spec = rootNode.accept(new CustomRsqlVisitor<SearchCriteria>());
        logger.debug("spec:" + spec);
        //logger.debug("Class:" + spec.getClass().getName());
    }
}
